#!/usr/bin/env perl

use warnings;
use strict;
use Getopt::Long;
use DBI;

my $bg = 0;
my $bgerr = 0;
my $graph = 0;
my $filename = "graph.png";
my $db_dump = 0;

# displays my help if there are no arguments or one of the specified options
usage() if (@ARGV < 1 or ! GetOptions('a=i' => \$bg, 'g' => \$graph, 'd=i' => \$bgerr, 'f=s' => \$filename, 'p' => \$db_dump));

# TO DO: print more useful error messages for bg and bg err
if ($bg >= 20 && $bg <= 600 && $bgerr == 0) { my $dbh = dbconnect(); bgadd($dbh,$bg); }
elsif ($bg != 0 && ($bg < 20 || $bg > 600)) { print "Can't add and remove a bg at the same time\n"; exit; }
if ($bgerr >= 0 && $bg == 0) { my $dbh = dbconnect(); bgoops($dbh,$bgerr); }
elsif ($bg != 0 && ($bgerr != 0 || $bgerr < 0)) { print "Can't have negative bgerr and add a bg\n"; exit; }
if ($db_dump == 1) { my $dbh = dbconnect(); db_dump($dbh); }
if ($graph == 1) { my $dbh = dbconnect(); grapher($dbh,$filename); }

# DONE
sub usage {
	use Term::ANSIColor qw(:constants);
	print "Unknown option: @_\n" if ( @_ );
	print "usage: $0 [ ", BOLD, "-a #", RESET, " ] [ ", BOLD, "-d #", RESET, " ] [ ", BOLD, "-g -f file", RESET, " ] [ ", BOLD "-p", RESET, " ]\n\n";
	print "\t", BOLD, "-a", RESET, "   :   Add a blood sugar value between 20 and 600\n";
	print "\t", BOLD, "-d", RESET, "   :   Delete the last X rows from the BG database\n"; 
	print "\t", BOLD, "-f", RESET, "   :   Specify output PNG filename. Must be used with ", BOLD, "-g", RESET, ". Default name is graph.png\n";
	print "\t", BOLD, "-g", RESET, "   :   Utilize graphing feature\n";
	print "\t", BOLD, "-p", RESET, "   :   Print all fields from the BG database\n\n";
	exit;
}

# DONE
sub dbconnect { 
	my $dsn = "DBI:SQLite:dbname=bgs.db";
	my $dbh = DBI->connect($dsn,'','',{ RaiseError => 1 }) or die $DBI::errstr;
	return $dbh;
}

# DONE
sub db_dump {
	# shift the passed in dbh value into the local dbh variable
	# TIL when variables are passed into a sub, they come in as an array
	my $dbh = shift;
	my $sth = $dbh->selectall_arrayref("SELECT * FROM bgtable");
	print "ID\tBG\tTimeStamp\n";
	print "---------------------------\n";
	foreach (@$sth) {
		my ($id, $bg, $timestamp) = @$_;
		print "$id\t$bg\t$timestamp\n";
		}
	$dbh->disconnect();
}

# DONE
sub bgadd {
	my $dbh = shift;
	my $bg = shift;
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
	$year += 1900;
	$mon += 1;
	if ($mon < 10) { $mon = "0".$mon; }
	if ($mday < 10) { $mday = "0".$mday; }
	if ($hour < 10) { $hour = "0".$hour; }
	my $ts = $year.$mon.$mday.$hour.$min;
	my $sth = $dbh->do("INSERT INTO bgtable(Bg, TimeStamp) VALUES ($bg, $ts)");
	$dbh->disconnect();
}

# DONE
sub bgoops {
	my $dbh = shift;
	my $bgerr = shift;
	for(my $i = 1;$i <= $bgerr;$i++) {
		my $sth = $dbh->do("DELETE FROM bgtable WHERE Id = (SELECT MAX(Id) FROM bgtable)");
		if ($i == $bgerr && $i > 1) { print "Removed: $i records\n"; }
		elsif ($i == $bgerr && $i == 1) { print "Removed: $i record\n"; }
		}
	$dbh->disconnect();
}

# DONE
sub grapher {
        # has dependancy: apt-get install libcairo2-dev
	# https://github.com/gphat/chart-clicker-examples/tree/master
	# http://search.cpan.org/~gphat/Chart-Clicker/lib/Chart/Clicker/Axis.pm#add_to_tick_values
	# http://varlogrant.blogspot.com/2010/01/making-pretty-pictures.html
	# for the skeleton of this grapher subroutine
	
	use Chart::Clicker;
	use Chart::Clicker::Context;
	use Chart::Clicker::Data::DataSet;
	use Chart::Clicker::Data::Marker;
	use Chart::Clicker::Data::Range;	
	use Chart::Clicker::Data::Series;
	use Geometry::Primitive::Rectangle;
	use Graphics::Color::RGB;
	use Geometry::Primitive::Circle;


	my $cc = Chart::Clicker->new(width => 900, height => 450, format => 'png');
	
	# Get information for the chart from my DB
	my $dbh = shift;
	my (@bgs, @timestamps);
	my $total=0;
	my $recordcount=0;
   	my $sth = $dbh->prepare("SELECT Bg, TimeStamp FROM bgtable ORDER BY TimeStamp ASC");
	$sth->execute();
	while ( my ($tempbg, $tempts) = $sth->fetchrow_array()) {
		$recordcount++;
		$total+=$tempbg;
		push (@bgs, $tempbg);
		push (@timestamps, $tempts);
		}
	$dbh->disconnect();

	# This stuff lets me get pretty dates for the X axis
	my @dateticks = @timestamps;
	my @junkarray;                                                                 
	foreach my $junkvar (@dateticks) { $junkvar = substr($junkvar,0,8); push (@junkarray, $junkvar);}
	my %temphash = map { $_, 0} @junkarray;                                                         
	my @unsorted = keys %temphash;                                                                    
	my @sorted = sort { $a <=> $b } @unsorted;
	foreach (@sorted) { print "$_\n"; }
	
	# Get average BGs of the entered data
	$total = $total/$recordcount;
	
	# Sets dynamic range of the X axis
	my $domain = Chart::Clicker::Data::Range->new(
       		lower => $timestamps[0],
        	upper => $timestamps[-1]
	);

	my $bloodsugar_chart = Chart::Clicker::Data::Series->new(
   		keys    => \@timestamps,
    		values  => \@bgs,
		name	=> "Bloodsugars"
		);

	# Display avg line for all the data in the DB, it is not the real avg of my bloodsugars
	# but an average of the data presented. I mainly check when I feel like crap/garbage/off
	my $avg_of_data = Chart::Clicker::Data::Series->new(
	   	keys    => \@timestamps,
	   	values  => [ ($total) x $recordcount ],
		name 	=> "Average"
		);
	
	# Any data below this line indicates when I had a low bloodsugar
	my $lowline = Chart::Clicker::Data::Series->new(
		keys	=> \@timestamps,
		values	=> [ (80) x $recordcount ],
		name	=> "Low range"
		);
	
	# Any data above this line indicates when I had a high bloodsugar
	my $highline = Chart::Clicker::Data::Series->new(
		keys	=> \@timestamps,
		values	=> [ (150) x $recordcount ],
		name	=> "High range"
		);

	$cc->background_color(Graphics::Color::RGB->new(red => .89, green => .89, blue => .89));
	$cc->plot->grid->background_color->alpha(0);

	# Color is in terms of rgb %
	my $graphcolor = Graphics::Color::RGB->new(red => .26, green => .50, blue => .26, alpha => 1);
	my $avgcolor = Graphics::Color::RGB->new(red => .88, green => .56, blue => .18, alpha => 1);
	my $lowcolor = Graphics::Color::RGB->new(red => .15, green => .25, blue => .9, alpha => 1);
	my $highcolor = Graphics::Color::RGB->new(red => 1, green => .10, blue => .05, alpha => 1);
	$cc->color_allocator->colors([ $graphcolor, $avgcolor, $lowcolor, $highcolor ]);

	my $ds = Chart::Clicker::Data::DataSet->new(series => [ $bloodsugar_chart, $avg_of_data, $lowline, $highline ]);
	$cc->title->text('My Bloodsugars Over Time');
	$cc->title->font->size(16);
	$cc->title->padding->bottom(5);
	$cc->grid_over(0);
	$cc->legend->visible(1);
	$cc->add_to_datasets($ds);

	my $defctx = $cc->get_context('default');
	$defctx->range_axis->format('%d');
	$defctx->range_axis->label('Bloodsugars');
	$defctx->domain_axis->label('Dates');
	$defctx->domain_axis->range($domain);
	#$defctx->domain_axis->ticks($#sorted + 1);
	# The line below will put fancy looking dates on the X axis but doesn't display them all
	# I don't know why yet.. so whatever, deal with dates+timestamps
	$defctx->domain_axis->tick_labels(\@sorted);
	$defctx->domain_axis->tick_values($bloodsugar_chart->keys);
	$defctx->domain_axis->clear_tick_values;	
	$defctx->domain_axis->tick_label_angle(.9);
	$defctx->domain_axis->padding->top(5);
	$defctx->domain_axis->format('%d');
	$defctx->range_axis->fudge_amount(.025);
	$defctx->renderer->brush->width(2);
	$cc->write_output("/var/www/assets/img/$filename");
}

